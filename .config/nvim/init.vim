lua require('plugins')

lua require('cmp_setup')
lua require('lsp_setup')
lua require('lspsaga_setup')

lua require('git').setup{default_mappings = true}

let mapleader = '\'
map \sf :Lspsaga finder<cr>
map \so :Lspsaga outline<cr>

colorscheme peachpuff
hi DiagnosticHint guifg=Blue
set guifont="Dejavu Sans Mono:h16"
