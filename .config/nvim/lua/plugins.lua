 -- vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function()
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'
  use 'nvim-neorg/neorg'
  use 'asvetliakov/vscode-neovim'
  use 'neovim/nvim-lspconfig'
  use 'hrsh7th/cmp-nvim-lsp'
  use 'hrsh7th/cmp-buffer'
  use 'hrsh7th/cmp-path'
  use 'hrsh7th/cmp-cmdline'
  use 'simrat39/rust-tools.nvim'
  use {
	  'nvimdev/lspsaga.nvim',
  }
  use {
	  'hrsh7th/nvim-cmp',
	  requires = {
		  'quangnguyen30192/cmp-nvim-tags'
	  },
	  config = function ()
		  require'cmp'.setup {
			  sources = {
				  { name = 'tags' },
				  -- more sources
			  }
		  }
	  end
  }		  
  use {
	  'dinhhuy258/git.nvim'
  }
  use {
       vim.fn.getcwd() .. '/.config/nvim/',
       as = 'local_conf',
       run = 'cp -r init.vim ginit.vim lua '..vim.fn.stdpath("config")
  }
end)
