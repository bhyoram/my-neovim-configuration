local ensure_packer = function()
  local fn = vim.fn
  local packer_path = fn.stdpath('data')..'/site/pack/packer/opt'
  local checkout_path = packer_path..'/packer.nvim'
  if fn.empty(fn.glob(checkout_path)) == false then
    return false
  end
  fn.system({'mkdir', '-p', packer_path})
  fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', checkout_path})
  vim.cmd [[packadd packer.nvim]]
  return true
end

local sync = function()
  dofile('.config/nvim/lua/plugins.lua')
  require('packer').sync()
end
vim.api.nvim_create_user_command('Resync', function(cmd)
    sync()
  end,
  {}
)

local packer_bootstrap = ensure_packer()

if packer_bootstrap then
  sync()
end
return true
