# My neovim configuration

## Name
My neovim configuration


## Description
bootstrap packer.vim and installs my prefered noevim plugins
can install my private configuration using packer

## Installation
./install.sh 

for silent installation (witout opening packer editing) run
./install -es

## Usage

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
